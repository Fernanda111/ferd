from django.db import models
from django.contrib.auth.models import User


class Diagrama(models.Model):
    titulo = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=500)
    fecha_creacion = models.DateField(auto_now_add=True)
    ultima_modificacion = models.DateTimeField(auto_now=True)
    # Se agregó un tipo de diagrama para poder diferenciarlos
    tipo = models.IntegerField(default=1)
    datos = models.TextField()
    autor = models.ForeignKey(User, on_delete=models.CASCADE, default="")

    class Meta:
        ordering = ['-fecha_creacion']

    def _str_(self):
        return self.titulo

from django import forms
from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from django.contrib.auth.models import User
from django.db import models
from django.forms import fields
from django.contrib import messages
from .models import Diagrama
from django.contrib.auth.forms import ReadOnlyPasswordHashField


class Registro_de_usuarios(UserCreationForm):
    username = forms.CharField(min_length=3, max_length="20")
    first_name = forms.CharField(min_length=3, max_length="20")
    last_name = forms.CharField(min_length=3, max_length="20")
    email = forms.EmailField(help_text='A valid email address, please.')


    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']

class DiagramaForm(forms.ModelForm):

    class Meta:
        model = Diagrama
       # fields = ['id','titulo', 'descripcion', 'autor', 'datos']
        exclude = ['autor']

class Actualizar_perfil(forms.ModelForm):
    
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_pwd = forms.CharField(widget=forms.PasswordInput())
    
    class Meta():
        model = User
        fields = ['first_name','last_name','username','email','password']
        help_texts={
            'username': None
        }
    
    def clean(self):
        cleaned_data = super(Actualizar_perfil, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_pwd")
        print(confirm_password)

        if password != confirm_password:
            print("yes")
            raise forms.ValidationError(
                "password and confirm_password does not match"
            )
  
